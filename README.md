#Frontend app
- Response render UI
- Connect to backend via APIs at: http://api.cportal.monster


#Code structure (from ./src)
- _api: API query functions as modules
- _components: UI Component
- _helper: Helper functions as modules
- _provider: Provider modules, contain functions can wrap other UI components
- _styles: All .css files

#Testing information
- Manager user: phucnguyen.snow@gmail.com / 123456
- Employee users: {user_name}@gmail.com / 123456
- Available users name: mark, phil, kylie, robb, vince, jake, ostra

