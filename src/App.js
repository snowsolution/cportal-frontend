import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import Login from "./_components/Login";
import Master from "./_components/Layout/Master";
import Dashboard from "./_components/Page/Dashboard";
import About from "./_components/Page/About";
import Users from "./_components/Page/Users";
import LeaveRequest from "./_components/Page/LeaveRequest";
import PrivateRoute from "./_components/PrivateRoute";
import './_styles/base.css';
import './_styles/all.css';
import './_styles/shadow.css';
import './_styles/expand-row.css';
import './_styles/data-modal.css';
import './_styles/fullcalendar.css';
import "react-datepicker/dist/react-datepicker.css";

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/about" element={<About/>}/>
        <Route path="/" element={(
          <PrivateRoute>
            <Master>
              <Dashboard/>
            </Master>
          </PrivateRoute>
        )}/>
        <Route path="/dashboard" element={(
          <PrivateRoute>
            <Master>
              <Dashboard/>
            </Master>
          </PrivateRoute>
        )}/>
        <Route path="/leave-request" element={(
          <PrivateRoute>
            <Master>
              <LeaveRequest/>
            </Master>
          </PrivateRoute>
        )}/>
        <Route path="/users" element={(
          <PrivateRoute>
            <Master>
              <Users/>
            </Master>
          </PrivateRoute>
        )}/>
        <Route path="/login" element={<Login/>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default App
