export const AuthApi = {
    isAuthenticated: false,
    signIn(callback) {
        AuthApi.isAuthenticated = true;
        setTimeout(callback, 500); // fake async
    },
    signOut(callback) {
        AuthApi.isAuthenticated = false;
        setTimeout(callback, 100);
    }
};