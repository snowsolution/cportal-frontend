import {Config} from "../_helper/Config";
import {AuthHeader} from "../_helper/AuthHeader";
import UrlEncode from "../_helper/UrlEncode";

export const LeaveRequestApi = {
  create,
  getAll,
  getAllApproved,
  getAssets,
  update,
  updatePartial
}

function getAllApproved() {
  const options = {
    method: 'POST',
    headers: AuthHeader.getAuthHeader(),
    mode: 'cors',
    body: UrlEncode({
      status: 'Approved'
    })
  }
  return fetch(`${Config.leaveRequestApiUrl()}/filter`, options).then(response => response.json())
}
function getAll(payload = null) {
  let options = {
    method: 'POST',
    headers: AuthHeader.getAuthHeader(),
    mode: 'cors'
  }
  if (payload) {
    options.body = UrlEncode(payload)
  }
  return fetch(Config.leaveRequestApiUrl(), options).then(response => response.json())
}

function create(payload) {
  const formData = UrlEncode(payload)
  return fetch(`${Config.leaveRequestApiUrl()}/add` , {
    method: 'POST',
    headers: AuthHeader.getAuthHeader(),
    mode: 'cors',
    body: formData
  }).then(response => response.json())
}

function update(payload) {
  const formData = UrlEncode(payload)
  return fetch(Config.leaveRequestApiUrl(), {
    method: 'PUT',
    headers: AuthHeader.getAuthHeader(),
    mode: 'cors',
    body: formData
  }).then(response => response.json())
}

function updatePartial(payload) {
  const formData = UrlEncode(payload)
  return fetch(Config.leaveRequestApiUrl(), {
    method: 'PATCH',
    headers: AuthHeader.getAuthHeader(),
    mode: 'cors',
    body: formData
  }).then(response => response.json())
}

function getAssets() {
  return fetch(Config.leaveRequestApiUrl(), {
    method: 'OPTIONS',
    headers: AuthHeader.getAuthHeader(),
    mode: 'cors',
  }).then(response => response.json())
}
