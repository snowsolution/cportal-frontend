import {Config} from "../_helper/Config";
import {AuthHeader} from "../_helper/AuthHeader";
import UrlEncode from "../_helper/UrlEncode";

export const UserApi = {
    login,
    register,
    getAll
}

function login(email, password) {
    const payload = {
        email: email,
        password: password
    }
    let formData = UrlEncode(payload)
    return fetch(Config.loginApiUrl(), {
        method : 'POST',
        headers : AuthHeader.getCasualHeader(),
        mode: 'cors',
        body: formData
    }).then(response => response.json())
}

function register (payload) {

}

function getAll() {
    return fetch(Config.userApiUrl(), {
        method: 'GET',
        headers: AuthHeader.getAuthHeader(),
        mode: 'cors',
    }).then(response => response.json())
}
