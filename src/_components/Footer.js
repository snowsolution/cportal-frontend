import '../_styles/footer.css'
function Footer() {
    return (
        <div className="footer">
            <span className="copyright">SecommSC &copy; 2021</span>
        </div>
    )
}

export default Footer