import {useState} from "react";

function Greeting() {
    const userName = useState('Demo user')

    return (
        <div className="greeting-block">
            <span>Welcome {userName}</span>
        </div>
    )
}
export default Greeting