import Navigation from "../Navigation";
import MobileNavigation from "../MobileNavigation";
import TopBar from "../TopBar";
import Footer from "../Footer";
import {useEffect, useState} from "react";
import $ from 'jquery';

function Master({children, onLogout}) {
  const [displayNav, setDisplayNav] = useState(false)
  useEffect(() => {
    setDisplayNav(window.innerWidth > process.env.REACT_APP_MOBILE_BREAKPOINT)
    window.addEventListener('resize', handleResize)
  }, [])
  const handleResize = () => {
    const bool = window.innerWidth > process.env.REACT_APP_MOBILE_BREAKPOINT
    setDisplayNav(bool)
    if (bool) {
      $('#mobileMenu').hide()
    }
  }

  const handleToggleMobileNav = () => {
    $('#mobileMenu').animate({width:'toggle'}, 100)
  }
  return (
    <div className={`app-container ${!displayNav ? 'mobile' : ''}`}>
      {displayNav && <Navigation/>}
      <TopBar onLogout={onLogout} onToggleMobileNav={handleToggleMobileNav}/>
      <MobileNavigation style={{
        display: 'none'
      }}/>
      <div className="content">
        {children}
      </div>
      <Footer/>
    </div>
  )
}

export default Master
