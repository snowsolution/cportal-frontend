import {Form, Card, Button} from 'react-bootstrap'
import {useEffect, useState} from "react";
import {Storage} from "../../_helper/Storage";
import {LeaveRequestApi} from "../../_api/LeaveRequestApi";
import {OptionArray} from "../../_helper/OptionArray";
import '../../_styles/calendar.css'
import DatePicker from "react-datepicker";
import {DateFormatter} from "../../_helper/DateFormatter";
import ErrorsBag from "../Ui/ErrorsBag";
import Spinner from "../Ui/Spinner";

function CreateModal({onCloseModal, onSubmit}) {
  const [showSpinner, setShowSpinner] = useState(false)
  const [types, setTypes] = useState([])
  const [type, setType] = useState()
  const [reason, setReason] = useState('')
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [errors, setErrors] = useState([])

  useEffect(() => {
    LeaveRequestApi.getAssets().then((data) => {
      const typeOption = OptionArray.toOptionArray(data.leave_request_type)
      setTypes(typeOption)
    })
    console.log(startDate)
  }, [])

  const handleCloseModal = () => {
    onCloseModal()
  }

  const handleSubmit = () => {
    const currentUser = JSON.parse(Storage.getUserInfo())
    const payload = {
      user_id: currentUser.id,
      status: 'Pending',
      type: type,
      reason: reason,
      from_date: DateFormatter.databaseFormat(startDate),
      to_date: DateFormatter.databaseFormat(endDate),
      manager_comment: '' //initial of a leave request will leave this field blank
    }
    setShowSpinner(true)
    LeaveRequestApi.create(payload).then((data) => {
      setShowSpinner(false)
      if (data.status) {
        setErrors([])
        onCloseModal()
        onSubmit()
      } else {
        if (data.error.length > 0) {
          setErrors(data.error)
        }
      }
    })
  }

  return (
    <div className="crud-modal">
      <span className="modal-mask"/>
      <Card className="card-modal">
        <Card.Body>
          <Card.Title>New leave request</Card.Title>
          <Form className="row">
            <div className="col-xs-12 col-sm-12 group-input">
              <Form.Label htmlFor="type">Type</Form.Label>
              <Form.Select required id="type" onChange={(e) => setType(e.target.value)} value={type}>
                <option key="-" value="0">Select...</option>
                {types.map(type => (
                  <option key={type.value} value={type.value}>{type.label}</option>
                ))}
              </Form.Select>
              <Form.Control.Feedback type="invalid">
                Please select a type
              </Form.Control.Feedback>
            </div>
            <div className="col-xs-12 col-sm-6 group-input">
              <Form.Label htmlFor="coin_id">From date</Form.Label>
              <DatePicker dateFormat="dd-MM-yyyy" selected={startDate}
                          onChange={(date) => setStartDate(date)}/>
            </div>
            <div className="col-xs-12 col-sm-6 group-input">
              <Form.Label htmlFor="coin_id">To date</Form.Label>
              <DatePicker dateFormat="dd-MM-yyyy" selected={endDate}
                          onChange={(date) => setEndDate(date)}/>
            </div>
            <div className="col-xs-12 col-sm-12 group-input">
              <Form.Label htmlFor="amount">Reason</Form.Label>
              <Form.Control value={reason} onChange={(e) => setReason(e.target.value)} id="amount"
                            as="textarea"/>
            </div>
            <div className="col-xs-12 col-sm-12 group-input">
              <ErrorsBag errors={errors}/>
            </div>
          </Form>
          <div className="form-action mt-3">
            <Button onClick={handleSubmit}>Submit</Button>
            <Button onClick={handleCloseModal} variant={"secondary"}>Cancel</Button>
          </div>
        </Card.Body>
        {showSpinner && <Spinner mask={true} size="60"/>}
      </Card>
    </div>
  )
}

export default CreateModal
