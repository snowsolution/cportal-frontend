import {Card} from "react-bootstrap";
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import {useEffect, useState} from "react";
import {LeaveRequestApi} from "../../../_api/LeaveRequestApi";
function Calendar({className = '', contentClass}) {
    const [calendarData, setCalendarData] = useState([])
    useEffect(() => {
      LeaveRequestApi.getAllApproved().then((data) => {
        let preparedData = []
        data.map(rowData => (
          preparedData.push({
            'title' : `${rowData.name} on ${rowData.type}`,
            'start': new Date(rowData.from_date),
            'end': new Date(rowData.to_date),
          })
        ))
        setCalendarData(preparedData)
      })
    }, [])
    return (
        <div className={`${className} ${contentClass}-tile`}>
            <Card className={`dashboard-calendar-wrapper ${contentClass}`}>
                <Card.Body>
                    <Card.Title>General calendar</Card.Title>
                    <FullCalendar
                        plugins={[ dayGridPlugin ]}
                        initialView="dayGridMonth"
                        events={calendarData}
                        height="500px"
                        themeSystem="sandstone"
                    />
                </Card.Body>

            </Card>
        </div>
    )
}

export default Calendar
