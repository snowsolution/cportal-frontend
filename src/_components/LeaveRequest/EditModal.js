import {Form, Card, Button} from 'react-bootstrap'
import {useEffect, useState} from "react";
import {LeaveRequestApi} from "../../_api/LeaveRequestApi";
import {OptionArray} from "../../_helper/OptionArray";
import '../../_styles/calendar.css'
import DatePicker from "react-datepicker";
import {DateFormatter} from "../../_helper/DateFormatter";
import ErrorsBag from "../Ui/ErrorsBag";
import Spinner from "../Ui/Spinner";

function EditModal({data, onCloseModal, onSubmit, isManager}) {
  const [showSpinner, setShowSpinner] = useState(false)
  const [types, setTypes] = useState([])
  const [statuses, setStatuses] = useState([])
  const [id, setId] = useState(data ? data.id : '')
  const [userId, setUserId] = useState(data ? data.user_id : '')
  const [type, setType] = useState(data ? data.type : '')
  const [status, setStatus] = useState(data ? data.status : '')
  const [reason, setReason] = useState(data ? data.reason : '')
  const [startDate, setStartDate] = useState(new Date(data ? data.from_date : ''))
  const [endDate, setEndDate] = useState(new Date(data ? data.to_date : ''))
  const [errors, setErrors] = useState([])
  const rowData = {
    user_id: userId,
    status: status,
    reason: reason,
    type: type,
    from_date: startDate,
    to_date: endDate,
  }

  useEffect(() => {
    LeaveRequestApi.getAssets().then((data) => {
      const typeOption = OptionArray.toOptionArray(data.leave_request_type)
      const statusOption = OptionArray.toOptionArray(data.status)
      setTypes(typeOption)
      setStatuses(statusOption)
    })
    console.log(startDate)
  }, [])

  const handleCloseModal = () => {
    onCloseModal(rowData)
  }

  const handleSubmit = () => {
    setShowSpinner(true)
    const payload = {
      id: id,
      user_id: userId,
      type: type,
      reason: reason,
      from_date: DateFormatter.databaseFormat(startDate),
      to_date: DateFormatter.databaseFormat(endDate),
    }
    if (isManager) {
      payload.status = status
    }
    LeaveRequestApi.updatePartial(payload).then((data) => {
      setShowSpinner(false)
      if (data.status) {
        setErrors([])
        onCloseModal(rowData)
        onSubmit()
      } else {
        if (data.error.length > 0) {
          setErrors(data.error)
        }
      }


    })
  }

  return (
    <div className="crud-modal">
      <span className="modal-mask"/>
      <Card className="card-modal">
        <Card.Body>
          <Card.Title>
            <Form className="row">
              <div className="col-xs-12 col-sm-6">
                <Form.Label htmlFor="type">Type</Form.Label>
                <Form.Select id="type" onChange={(e) => setType(e.target.value)} value={type}>
                  <option key="-" value="0">Select...</option>
                  {types.map(type => (
                    <option key={type.value} value={type.value}>{type.label}</option>
                  ))}
                </Form.Select>
              </div>
              <div className="col-xs-12 col-sm-6">
                <Form.Label htmlFor="coin_id">Status</Form.Label>
                <Form.Select disabled={!isManager} id="status" onChange={(e) => setStatus(e.target.value)} value={status}>
                  <option key="-" value="0">Select...</option>
                  {statuses.map(status => (
                    <option key={status.value} value={status.value}>{status.label}</option>
                  ))}
                </Form.Select>
              </div>
              <div className="col-xs-12 col-sm-6">
                <Form.Label htmlFor="coin_id">From date</Form.Label>
                <DatePicker dateFormat="dd-MM-yyyy" selected={startDate} onChange={(date) => setStartDate(date)}/>
              </div>
              <div className="col-xs-12 col-sm-6">
                <Form.Label htmlFor="coin_id">To date</Form.Label>
                <DatePicker dateFormat="dd-MM-yyyy" selected={endDate} onChange={(date) => setEndDate(date)}/>
              </div>
              <div className="col-xs-12 col-sm-12">
                <Form.Label htmlFor="amount">Reason</Form.Label>
                <Form.Control value={reason} onChange={(e) => setReason(e.target.value)} id="amount"
                              as="textarea"/>
              </div>
              <div className="col-xs-12 col-sm-12 group-input">
                <ErrorsBag errors={errors}/>
              </div>
            </Form>
            <div className="form-action mt-3">
              <Button onClick={handleSubmit}>Submit</Button>
              <Button onClick={handleCloseModal} variant={"secondary"}>Cancel</Button>
            </div>
          </Card.Title>
        </Card.Body>
        {showSpinner && <Spinner mask={true} size="60"/>}
      </Card>
    </div>
  )
}

export default EditModal
