import {Button, Form} from "react-bootstrap";
import {DateFormatter} from "../../_helper/DateFormatter";
import {useState} from "react";
function ExpandRow({data, onChange}) {
  const [managerComment, setManagerComment] = useState(data.manager_comment ? data.manager_comment : '')
  const [disableControl, setDisableControl] = useState(true)

  const handleSubmit = () => {
    const payload = {
      id: data.id,
      manager_comment: managerComment
    }
    setDisableControl(!disableControl)
    onChange(payload)
  }

  const handleEdit = () => {
    setDisableControl(!disableControl)
  }

  return (
    <div className="expand-row-wrapper row">
      <div className="col-xs-12 col-sm-6">
        <div className="expand-block-title">Manager comment</div>
        <div className="expand-block-content">
          <Form.Control as="textarea" row="3"
                        value={managerComment}
                        onChange={(e) => setManagerComment(e.target.value)}
                        disabled={disableControl}
          />
          {disableControl ? <Button onClick={handleEdit}>Edit comment</Button> : <Button onClick={handleSubmit}>Save</Button>}
        </div>
      </div>
      <div className="col-xs-12 col-sm-6">
        <div className="row">
          <div className="expand-block col-xs-12 col-sm-6">
            <div className="expand-block-title">From date</div>
            <div className="expand-block-content">
              {DateFormatter.displayFormat(new Date(data.from_date))}
            </div>
          </div>
          <div className="expand-block col-xs-12 col-sm-6">
            <div className="expand-block-title">To date</div>
            <div className="expand-block-content">
              {DateFormatter.displayFormat(new Date(data.to_date))}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="expand-block col-xs-12 col-sm-12">
            <div className="expand-block-title">Reason</div>
            <div className="expand-block-content">
              {data.reason}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ExpandRow
