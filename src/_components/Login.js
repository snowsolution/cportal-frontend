import {Card, Button} from 'react-bootstrap'
import Spinner from "./Ui/Spinner";
import Logo from "./Logo";
import {useState, memo, useEffect} from "react";
import {Storage} from "../_helper/Storage";
import {UserApi} from '../_api/UserApi'
import {Navigate, useNavigate} from "react-router-dom";
import '../_styles/login.css'
import ErrorsBag from "./Ui/ErrorsBag";

function Login() {
  const navigate = useNavigate()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [controlDisable, setControlDisable] = useState(false)
  const [showSpinner, setShowSpinner] = useState(false)
  const [errors, setErrors] = useState([])


  // useEffect(() => {
  //   window.addEventListener('keypress', handleEnterLogin)
  //
  //   return () => {
  //     window.removeEventListener('keypress', handleEnterLogin)
  //   }
  // }, [])
  //
  // const handleEnterLogin = (e) => {
  //   if (e.key === 'Enter') {
  //     handleLogin()
  //   }
  // }

  const handleLogin = () => {
    setControlDisable(true)
    setShowSpinner(true)
    console.log(email, password)
    UserApi.login(email, password).then((data) => {
      setControlDisable(false)
      setShowSpinner(false)
      if (data.status) {
        setErrors([])
        Storage.setAuthenticateToken(data.token)
        Storage.setUserInfo(JSON.stringify(data.user))
        setTimeout(() => {
          navigate('/dashboard')
        }, 300)
      } else {
        setErrors([
          data.error
        ])
      }

    })
  }
  return (
    !Storage.getUserInfo() ?
      <div className="login-wrapper">
        <div className="login-form">
          <Card>
            <Card.Body>
              {showSpinner && <Spinner size={120} mask={false}/>}
              <Logo/>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input disabled={controlDisable} onChange={(e) => setEmail(e.target.value)}
                       className="form-control" type="text" id="email"/>
              </div>
              <div>
                <label htmlFor="password">Password</label>
                <input disabled={controlDisable} onChange={(e) => setPassword(e.target.value)}
                       className="form-control" type="password" id="password"/>
              </div>
              <ErrorsBag errors={errors}/>
              <div className="mt-3">
                <Button disabled={controlDisable} onClick={() => handleLogin()}>Login</Button>
              </div>
            </Card.Body>
          </Card>
        </div>
      </div>
      :
      <Navigate to="/dashboard"/>
  )
}

export default memo(Login)
