import { Image } from 'react-bootstrap'
function Logo() {
    return (
        <div className="logo-wrapper">
            <Image src="images/secomm-logo.svg" rounded />
            {/*<Image src="images/loading.gif" rounded />*/}
        </div>
    )
}

export default Logo