import '../_styles/mobile-navigation.css'
import NavigationItem from "./NavigationItem";
import Logo from "./Logo";
function MobileNavigation({style}) {
  return (
    <div id="mobileMenu" className="left-navigation mobile right-shadow" style={style}>
      <Logo/>
      <div className="nav-item-list">
        <NavigationItem href="/dashboard" label="Dashboard"/>
        <NavigationItem href="/leave-request" label="Leave Request"/>
        <NavigationItem href="/users" label="Users"/>
      </div>
    </div>
  )
}

export default MobileNavigation
