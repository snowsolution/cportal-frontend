import '../_styles/navigation.css'
import NavigationItem from "./NavigationItem";
import Logo from "./Logo";
function Navigation() {
    return (
        <div className="left-navigation right-shadow">
            <Logo/>
            <div className="nav-item-list">
                <NavigationItem href="/dashboard" label="Dashboard"/>
                <NavigationItem href="/leave-request" label="Leave Request"/>
                <NavigationItem href="/users" label="Users"/>
            </div>
        </div>
    )
}

export default Navigation