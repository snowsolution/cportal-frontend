import {Link} from "react-router-dom";
function NavigationItem({label, href}) {
    return (
        <div className="nav-item">
            <Link to={href}>{label}</Link>
        </div>
    )
}

export default NavigationItem