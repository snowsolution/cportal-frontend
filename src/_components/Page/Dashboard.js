import LeaveRequestCalendar from "../LeaveRequest/Dashboard/Calendar";
function Dashboard() {
    const widgets = [
        LeaveRequestCalendar,
    ]
    return (
        <div className="dashboard-wrapper row">
            {widgets.map((Widget, index) => (
                <Widget key={index} className="col-xs-12 col-md-12" contentClass="dashboard-widget"/>
            ))}
        </div>
    )
}

export default Dashboard