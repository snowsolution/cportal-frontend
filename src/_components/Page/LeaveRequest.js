import {useEffect, useState} from "react";
import {LeaveRequestApi} from "../../_api/LeaveRequestApi";
import DataTable from 'react-data-table-component';
import {Card} from "react-bootstrap";
import Spinner from "../Ui/Spinner";
import ActionLink from "../Ui/ActionLink";
import GridAction from "../Ui/GridAction";
import CreateModal from "../LeaveRequest/CreateModal";
import EditModal from "../LeaveRequest/EditModal";
import {DateFormatter} from "../../_helper/DateFormatter";
import ExpandRow from "../LeaveRequest/ExpandRow";
import {Storage} from "../../_helper/Storage";

function LeaveRequest() {
  const userInfo = JSON.parse(Storage.getUserInfo())
  const [displayNewModal, setDisplayNewModal] = useState(false)
  const [displayEditModal, setDisplayEditModal] = useState(false)
  const [editData, setEditData] = useState({})
  const [showSpinner, setShowSpinner] = useState(true)
  const [data, setData] = useState([])
  const [isManager, setIsManager] = useState(userInfo.role_id === 1 ? true : false)
  useEffect(() => {
    fetchData()
  }, [])

  const fetchData = () => {
    let payload = null
    if (!isManager) {
      payload = {
        user_id: userInfo.id
      }
    }
    LeaveRequestApi.getAll(payload).then((result) => {
      setData(result)
      setShowSpinner(false)
    })
  }

  const handleApprove = (row) => {
    const payload = {
      id: row.id,
      status: 'Approved'
    }
    LeaveRequestApi.updatePartial(payload).then((data) => {
      fetchData()
    })
  }

  const handleDecline = (row) => {
    const payload = {
      id: row.id,
      status: 'Declined'
    }
    LeaveRequestApi.updatePartial(payload).then((data) => {
      fetchData()
    })
  }

  const handleChange = (payload) => {
    LeaveRequestApi.updatePartial(payload).then((data) => {
      fetchData()
    })
  }

  const handleNew = () => {
    setDisplayNewModal(!displayNewModal)
  }

  const handleEdit = (row) => {
    setEditData(row)
    setDisplayEditModal(!displayEditModal)
  }

  const columns = [
    {
      name: '#',
      selector: row => row.id,
      sortable: true,
      width: '60px'
    },
    {
      name: 'User',
      selector: row => `${row.name} (${row.email})`,
      sortable: true,
    },
    {
      name: 'Type',
      selector: row => row.type,
      sortable: true,
      width: "150px"
    },
    {
      name: 'Created date',
      selector: row => DateFormatter.displayFormat(new Date(row.created_at)),
      sortable: true,
      width: "120px"
    },
    {
      name: 'Status',
      selector: row => row.status,
      sortable: true,
    },
    {
      name: 'Action',
      selector: row => {
        let actions = []
        if (isManager) {
          if (row.status === 'Pending') {
            actions = [
              {
                label: 'Approve',
                callback: handleApprove
              },
              {
                label: 'Decline',
                callback: handleDecline,
                className: 'warning'
              }
            ]
          }
          if (row.status === 'Approved') {
            actions = [
              {
                label: 'Decline',
                callback: handleDecline,
                className: 'warning'
              }
            ]
          }
          if (row.status === 'Declined') {
            actions = [
              {
                label: 'Approve',
                callback: handleApprove,
              }
            ]
          }
          actions.push(
            {
              label: 'Edit',
              callback: (row) => handleEdit(row)
            }
          )
        } else {
          actions = [
            {
              label: 'Edit',
              callback: (row) => handleEdit(row)
            }
          ]
        }
        return (
          <div className="list-action">
            {actions.map(action => (
              <ActionLink className={action.className ?? ''} key={action.label} label={action.label}
                          onClick={() => action.callback(row)}/>
            ))}
          </div>
        )
      }
    },

  ];

  const gridActions = [
    {
      label: "New",
      callback: handleNew,
    },
  ]
  return (
    <>
      <GridAction actions={gridActions}/>
      <Card className="data-list user-list">
        <Card.Body>
          {showSpinner && <Spinner size="60" mask={false}/>}
          <Card.Title>Leave request</Card.Title>
          {
            isManager ? <DataTable columns={columns} data={data} pagination expandableRows
                                   expandableRowsComponentProps={{onChange: handleChange}} expandableRowsComponent={ExpandRow}/>
              : <DataTable columns={columns} data={data} pagination/>
          }
        </Card.Body>
      </Card>
      {displayNewModal && <CreateModal onSubmit={fetchData} onCloseModal={handleNew}/>}
      {displayEditModal && <EditModal isManager={isManager} data={editData} onSubmit={fetchData} onCloseModal={handleEdit}/>}
    </>
  )
}

export default LeaveRequest
