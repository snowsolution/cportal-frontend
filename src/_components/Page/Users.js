import {useEffect, useState} from "react";
import {UserApi} from "../../_api/UserApi";
import DataTable from 'react-data-table-component';
import {Card} from "react-bootstrap";
import Spinner from "../Ui/Spinner";

function Users() {
    const [showSpinner, setShowSpinner] = useState(true)
    const [data, setData] = useState([])
    useEffect(() => {
        UserApi.getAll().then((result) => {
            setData(result)
            setShowSpinner(false)
        })
    }, [])
    const columns = [
        {
            name: '#',
            selector: row => row.id,
            sortable: true,
            width: '60px'
        },
        {
            name: 'Name',
            selector: row => row.name,
            sortable: true,
        },
        {
            name: 'Email',
            selector: row => row.email,
            sortable: true,
        },
        {
            name: 'Role',
            selector: row => row.role,
            sortable: true,
        },
        {
            name: 'Position',
            selector: row => row.position,
            sortable: true,
        },

    ];
    return (
        <Card className="data-list user-list">
            <Card.Body>
                {showSpinner && <Spinner size="60" mask={false}/>}
                <Card.Title>All users</Card.Title>
                <DataTable columns={columns} data={data} pagination/>
            </Card.Body>
        </Card>
    )
}
export default Users
