import { Navigate } from 'react-router-dom';
import {Storage} from "../_helper/Storage";
function PrivateRoute({children}) {
    const isLoggedIn = !!Storage.getAuthenticateToken()
    return isLoggedIn ? children : <Navigate to="/login" />;
}

export default PrivateRoute
