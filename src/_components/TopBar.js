import Dropdown from "./TopBar/Dropdown";
import {Image} from "react-bootstrap";
import '../_styles/top-bar.css'
import {useEffect, useState} from "react";

function TopBar({onLogout, onToggleMobileNav}) {
  const [showDropdown, setShowDropdown] = useState(false)
  const [displayMobileMenu, setDisplayMobileMenu] = useState(false)

  useEffect(() => {
    setDisplayMobileMenu(!(window.innerWidth > process.env.REACT_APP_MOBILE_BREAKPOINT))
    window.addEventListener('resize', handleResize)
  }, [])

  const handleResize = () => {
    setDisplayMobileMenu(!(window.innerWidth > process.env.REACT_APP_MOBILE_BREAKPOINT))
  }

  const handleShowDropdown = () => {
    setShowDropdown(!showDropdown)
  }

  const handleShowMobileNav = () => {
    onToggleMobileNav()
  }


  return (
    <div className="top-bar bottom-shadow">
      {displayMobileMenu && <span className="mobile-menu">
        <Image src="images/mobile-menu.jpeg" onClick={handleShowMobileNav}/>
      </span>}
      <span className="user-avatar">
        <Image src="images/user.png" onClick={handleShowDropdown}/>
      </span>
      {showDropdown && <Dropdown/>}
    </div>
  )
}

export default TopBar
