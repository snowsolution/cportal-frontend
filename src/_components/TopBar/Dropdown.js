import {Card, Button} from "react-bootstrap";
import {Storage} from "../../_helper/Storage";
import {useNavigate} from "react-router-dom";

function Dropdown() {
    const navigate = useNavigate()
    const userInfo = JSON.parse(Storage.getUserInfo())

    const handleLogout = () => {
        Storage.removeAuthenticateToken()
        navigate('/login')
    }

    return (
        <Card className="topBar-dropdown box-light-shadow">
            <Card.Body>
                <div className="topBar-user-info">
                    <div>{userInfo.name}</div>
                    <div className="user-role">{userInfo.role}</div>
                </div>
                <div className="topBar-item">
                    <Button className="btn-logout" variant="secondary" onClick={handleLogout}>Logout</Button>
                </div>
            </Card.Body>
        </Card>
    )
}
export default Dropdown
