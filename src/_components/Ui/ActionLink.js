import '../../_styles/action-link.css'
function ActionLink({label, onClick, className}) {
    return (
        <span className={`action-link ${className}`} onClick={onClick}>{label}</span>
    )
}
export default ActionLink