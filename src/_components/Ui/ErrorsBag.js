import '../../_styles/error-bag.css'
import {memo} from "react";
function Error({error}) {
    return (
        <div className="error-bag-item">{error}</div>
    )
}

function ErrorsBag({errors}) {
    console.log('re-render error bag')
    return (
        <div className="error-bag">
            {
                errors.map((error, index) => (
                    <Error key={index} error={error}/>
                ))
            }
        </div>
    )
}



export default memo(ErrorsBag)