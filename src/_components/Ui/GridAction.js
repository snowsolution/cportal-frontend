import {Button, Card} from "react-bootstrap";
import {memo} from 'react'
function GridAction({actions}) {
    console.log('re-render grid action')
    return (
        <Card className="grid-action">
            <Card.Body>
                {actions.map((action, index) => (
                    <Button key={index} className="grid-action-btn" onClick={action.callback}>{action.label}</Button>
                ))}
            </Card.Body>
        </Card>
    )
}

export default memo(GridAction)