import { Image } from 'react-bootstrap'

function Spinner({size, mask = false}) {

    const getWrapperStyle = () => {
        let halfSize = size / 2
        return {
            top: `calc(50% - ${halfSize}px)`,
            left: `calc(50% - ${halfSize}px)`
        }
    }

    return (
        <div className={"spinner-wrapper"} style={getWrapperStyle()}>
            <Image style={{width: `${size}px`, height: `${size}px`}} src="images/loading.gif"/>
            {mask && <span className="spinner-mask"></span>}
        </div>
    )
}

export default Spinner