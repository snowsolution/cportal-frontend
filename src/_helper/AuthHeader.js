import {Storage} from "./Storage";

export const AuthHeader = {
    getAuthHeader,
    getCasualHeader
}
function getAuthHeader() {
    const authString = 'Bearer ' + Storage.getAuthenticateToken()
    return {
        'Authorization': authString,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Credentials': true
    }
}

function getCasualHeader() {
    return {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
    }
}
