export const Config = {
    apiBaseUrl,
    loginApiUrl,
    registerApiUrl,
    userApiUrl,
    leaveRequestApiUrl,
}


function apiBaseUrl() {
    return process.env.REACT_APP_API_HOST_URL
}
function loginApiUrl() {
    return `${apiBaseUrl()}/login`
}
function registerApiUrl() {
    return `${apiBaseUrl()}/register`
}
function userApiUrl() {
    return `${apiBaseUrl()}/user`
}
function leaveRequestApiUrl() {
    return `${apiBaseUrl()}/leave-request`
}