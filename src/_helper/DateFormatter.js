export const DateFormatter = {
  databaseFormat,
  displayFormat
}

function displayFormat(dateObject) {
  let formattedDate = ''
  let day = ''+ dateObject.getDate()
  let month = '' + (dateObject.getMonth() + 1)
  let year = dateObject.getFullYear()
  if (day.length < 2) {
    day = '0' + day
  }

  if (month.length < 2) {
    month = '0' + month
  }
  formattedDate = `${day}-${month}-${year}`
  return formattedDate
}

function databaseFormat(dateObject) {
    let formattedDate = ''
    let day = ''+ dateObject.getDate()
    let month = '' + (dateObject.getMonth() + 1)
    let year = dateObject.getFullYear()
    if (day.length < 2) {
        day = '0' + day
    }

    if (month.length < 2) {
        month = '0' + month
    }
    formattedDate = `${year}-${month}-${day}`
    return formattedDate
}
