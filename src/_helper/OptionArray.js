export const OptionArray = {
    toOptionArray,
    toArray
}

function toArray(object) {
    return Object.entries(object)
}

function toOptionArray(arrayObject, keyField = 'value', labelField = 'label') {
    if (typeof arrayObject === 'object') {
        arrayObject = OptionArray.toArray(arrayObject)
    }
    let optionArray = []
    arrayObject.map(e => (
        optionArray.push({
            [keyField]: e[0],
            [labelField]: e[1]
        })
    ))
    return optionArray
}