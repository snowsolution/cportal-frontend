export const Storage = {
    getAuthenticateToken,
    setAuthenticateToken,
    removeAuthenticateToken,
    getUserInfo,
    setUserInfo
}

function getAuthenticateToken() {
    return localStorage.getItem('token')
}

function setAuthenticateToken(token) {
    localStorage.setItem('token', token)
}

function removeAuthenticateToken() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
}

function getUserInfo() {
    return localStorage.getItem('user')
}

function setUserInfo(user) {
    localStorage.setItem('user', user)
}


