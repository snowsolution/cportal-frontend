function UrlEncode(payload)
{
    let formData = [];
    for (let property in payload) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(payload[property]);
        formData.push(encodedKey + "=" + encodedValue);
    }
    formData = formData.join("&");
    return formData
}

export default UrlEncode