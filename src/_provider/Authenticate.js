import React, {useState, useContext, createContext} from "react";
import {AuthApi} from "../_api/AuthApi";
let AuthContext = createContext();

function Authenticate ({ children }) {
    const auth = useProvideAuth();
    return (
        <AuthContext.Provider value={auth}>
            {children}
        </AuthContext.Provider>
    );
}

export function useAuth() {
    return useContext(AuthContext);
}

function useProvideAuth() {
    const [user, setUser] = useState(null);

    const signIn = cb => {
        return AuthApi.signIn(() => {
            setUser("user");
            cb();
        });
    };

    const signOut = cb => {
        return AuthApi.signOut(() => {
            setUser(null);
            cb();
        });
    };

    return {
        user,
        signIn,
        signOut
    };
}

export default Authenticate